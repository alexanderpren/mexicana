<?php
App::uses('AppModel', 'Model');
/**
 * Destino Model
 *
 * @property Tour $Tour
 */
class Destino extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'id_destino';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'destino';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Tour' => array(
			'className' => 'Tour',
			'foreignKey' => 'destino_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
