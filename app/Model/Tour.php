<?php
App::uses('AppModel', 'Model');
/**
 * Tour Model
 *
 * @property Destino $Destino
 * @property Cliente $Cliente
 */
class Tour extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'id_tour';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Destino' => array(
			'className' => 'Destino',
			'foreignKey' => 'destino_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Cliente' => array(
			'className' => 'Cliente',
			'foreignKey' => 'cliente_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Hotele' => array(
			'className' => 'Hotele',
			'foreignKey' => 'hotel_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
