<?php
App::uses('AppModel', 'Model');
/**
 * Hotele Model
 *
 */
class Hotele extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'id_hotel';
        var $displayField = 'nombre_hotel';

}
