<?php
App::uses('AppModel', 'Model');
/**
 * Nacionalidade Model
 *
 */
class Nacionalidade extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'id_nacionalidad';
        var $displayField = 'nacionalidad';

}
