<?php
App::uses('AppModel', 'Model');
/**
 * Idioma Model
 *
 */
class Idioma extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'id_idioma';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'idioma';

}
