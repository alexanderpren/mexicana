<?php
App::uses('AppModel', 'Model');
/**
 * Cliente Model
 *
 * @property Nacionalidad $Nacionalidad
 * @property Hotel $Hotel
 * @property Idioma $Idioma
 * @property Tour $Tour
 */
class Cliente extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'id_cliente';
        var $displayField = 'nombre';
        
      


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Nacionalidade' => array(
			'className' => 'Nacionalidade',
			'foreignKey' => 'nacionalidad_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Hotele' => array(
			'className' => 'Hotele',
			'foreignKey' => 'hotel_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Idioma' => array(
			'className' => 'Idioma',
			'foreignKey' => 'idioma_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Tour' => array(
			'className' => 'Tour',
			'foreignKey' => 'cliente_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
