<?php
/**
 * Cliente Fixture
 */
class ClienteFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id_cliente' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'nombre' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf32_spanish2_ci', 'charset' => 'utf32'),
		'nacionalidad_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'hotel_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'idioma_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf32_spanish2_ci', 'charset' => 'utf32'),
		'indexes' => array(
			'PRIMARY' => array('column' => array('id_cliente', 'nacionalidad_id', 'hotel_id'), 'unique' => 1),
			'fk_Cliente_Nacionalidad1_idx' => array('column' => 'nacionalidad_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf32', 'collate' => 'utf32_spanish2_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id_cliente' => 1,
			'nombre' => 'Lorem ipsum dolor sit amet',
			'nacionalidad_id' => 1,
			'hotel_id' => 1,
			'idioma_id' => 'Lorem ipsum dolor sit amet'
		),
	);

}
