<?php
/**
 * Nacionalidade Fixture
 */
class NacionalidadeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id_nacionalidad' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'nacionalidad' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf32_spanish2_ci', 'charset' => 'utf32'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id_nacionalidad', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf32', 'collate' => 'utf32_spanish2_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id_nacionalidad' => 1,
			'nacionalidad' => 'Lorem ipsum dolor sit amet'
		),
	);

}
