<?php
/**
 * Hotele Fixture
 */
class HoteleFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id_hotel' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'nombre_hotel' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf32_spanish2_ci', 'charset' => 'utf32'),
		'numero_habitacion' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'utf32_spanish2_ci', 'charset' => 'utf32'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id_hotel', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf32', 'collate' => 'utf32_spanish2_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id_hotel' => 1,
			'nombre_hotel' => 'Lorem ipsum dolor sit amet',
			'numero_habitacion' => 'Lorem ipsum dolor sit amet'
		),
	);

}
