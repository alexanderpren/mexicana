<?php
App::uses('AppController', 'Controller');
/**
 * Nacionalidades Controller
 *
 * @property Nacionalidade $Nacionalidade
 * @property PaginatorComponent $Paginator
 */
class NacionalidadesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Nacionalidade->recursive = 0;
		$this->set('nacionalidades', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Nacionalidade->exists($id)) {
			throw new NotFoundException(__('Invalid nacionalidade'));
		}
		$options = array('conditions' => array('Nacionalidade.' . $this->Nacionalidade->primaryKey => $id));
		$this->set('nacionalidade', $this->Nacionalidade->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Nacionalidade->create();
			if ($this->Nacionalidade->save($this->request->data)) {
				$this->Flash->success(__('La nueva nacionalidad ha sido guardada'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The nacionalidade could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Nacionalidade->exists($id)) {
			throw new NotFoundException(__('Invalid nacionalidade'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Nacionalidade->save($this->request->data)) {
				$this->Flash->success(__('La nacionalidad ha sido guardada'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The nacionalidade could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Nacionalidade.' . $this->Nacionalidade->primaryKey => $id));
			$this->request->data = $this->Nacionalidade->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Nacionalidade->id = $id;
		if (!$this->Nacionalidade->exists()) {
			throw new NotFoundException(__('Invalid nacionalidade'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Nacionalidade->delete()) {
			$this->Flash->success(__('The nacionalidade has been deleted.'));
		} else {
			$this->Flash->error(__('The nacionalidade could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
