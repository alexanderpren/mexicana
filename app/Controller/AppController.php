<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
  
    
    public $components = array(
        //'DebugKit.Toolbar',
        'Session',
        'Auth' => array
                (
                    'userModel'=>'Usuario',
                    'authenticate' => array
                                    (
                                        'Form' => array
                                                    (
                                                        //Asignacion de campos para el login y la contraseÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â±a, indespensable para el login
                                                        'fields' => array('username' => 'nombre_usuario',
                                                        'password'=>'password'),
                                                        //Modelo que utilizaremos para la autenticacion
                                                        'userModel'=>'Usuario',
                                                        //Condicion de logueo, unicamente se podran loguear los usuarios que esten activos
                                                        //'scope'=>array('CoUsuario.activo'=>1)
                                                        'passwordHasher' => 'Blowfish'
                                                    )
                                    ),
//                    'authorize' => array('Controller'),
                    //Hacia donde se redirigira para el login
                    'loginAction'=>array('controller'=>'usuarios','action'=>'login'),
                    //Mensaje en caso de login ioncorrecto
                    'loginError'=>"Nombre de usuario o contrase&ntilde;a incorrectos",
                    'loginRedirect'=> array('controller' => 'pages', 'action' => 'display', 'principal')
                )
    );
    
     public function beforeFilter() {
        //$this->Auth->allow('index','add');
    }
 
    
}
