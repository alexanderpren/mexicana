<div class="hoteles view">
<h2><?php echo __('Hotele'); ?></h2>
	<dl>
		<dt><?php echo __('Id Hotel'); ?></dt>
		<dd>
			<?php echo h($hotele['Hotele']['id_hotel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre Hotel'); ?></dt>
		<dd>
			<?php echo h($hotele['Hotele']['nombre_hotel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Numero Habitacion'); ?></dt>
		<dd>
			<?php echo h($hotele['Hotele']['numero_habitacion']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Hotele'), array('action' => 'edit', $hotele['Hotele']['id_hotel'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Hotele'), array('action' => 'delete', $hotele['Hotele']['id_hotel']), array('confirm' => __('Are you sure you want to delete # %s?', $hotele['Hotele']['id_hotel']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Hoteles'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Hotele'), array('action' => 'add')); ?> </li>
	</ul>
</div>
