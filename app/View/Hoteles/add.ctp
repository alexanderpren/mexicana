<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
<?php echo $this->Form->create('Hotele'); ?>
	<fieldset>
		<legend><?php echo __('Agregar Hotel'); ?></legend>
	<?php
		echo $this->Form->input('nombre_hotel');
		echo $this->Form->input('numero_habitacion');
	?>
	</fieldset>
<?php echo $this->Form->button('Agregar',array('type'=>'submit','class'=>'btn btn-primary')); ?>
    </div>
    <div class="col-md-4"></div>
</div>