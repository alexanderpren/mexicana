<div class="hoteles index">
	<h2><?php echo __('Hoteles'); ?></h2>
	<table  class="table" cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id_hotel'); ?></th>
			<th><?php echo $this->Paginator->sort('nombre_hotel'); ?></th>
			<th><?php echo $this->Paginator->sort('numero_habitacion'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($hoteles as $hotele): ?>
	<tr>
		<td><?php echo h($hotele['Hotele']['id_hotel']); ?>&nbsp;</td>
		<td><?php echo h($hotele['Hotele']['nombre_hotel']); ?>&nbsp;</td>
		<td><?php echo h($hotele['Hotele']['numero_habitacion']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $hotele['Hotele']['id_hotel'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $hotele['Hotele']['id_hotel'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $hotele['Hotele']['id_hotel']), array('confirm' => __('Are you sure you want to delete # %s?', $hotele['Hotele']['id_hotel']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Hotele'), array('action' => 'add')); ?></li>
	</ul>
</div>
