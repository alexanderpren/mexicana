<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */


//$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
//?>



    <!DOCTYPE html>
    <html lang="en">
        <head>
            <?php echo $this->Html->charset(); ?>
            <?php echo $this->Flash->render(); ?>
             
            <title>
		<?php // echo $cakeDescription ?>La Mexicana
		<?php // echo $this->fetch('title'); ?>
            </title>
            <?php
		echo $this->Html->meta('icon');
                echo $this->Html->script(array('jquery.min','bootstrap.min','jquery-clockpicker.min','moment.min','datetimepicker.min','datetimepicker.es'));
		echo $this->Html->css(array('bootstrap.min','bootstrap-theme.min','jquery-clockpicker.min','style','datepicker.min'));               
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="description" content="Navbar tutorial from BootstrapBay.com">
            <meta name="author" content="BootstrapBay.com">
            <title>Mexicana</title>
          </head>
        <body>
            <div class="row" style="padding-top: 40px;">
                <div class="col-md-12"><?php
                echo $this->Session->flash(); 
                        echo $this->fetch('content');               
                        ?></div>                
            </div>     
            <!-- Fixed navbar -->
            <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                 
                            </a>
              <div class="container">
                    <div class="navbar-header">                          
                       <a class="navbar-brand" href="#">LA MEXICANA
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"> <?php echo $this->Html->link('Home',array('controller' => 'pages', 'action' => 'display', 'principal')); ?></li>       
                            <li><?php echo $this->Html->link('Tours',array('controller'=>'tours','action'=>'index')); ?></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Administracion <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-header">Nacionalidades</li>
                                    <li><?php echo $this->Html->link('Lista Nacionalidades',array('controller'=>'nacionalidades','action'=>'index')); ?></li>
                                    <li><?php echo $this->Html->link('Nueva Nacionalidad',array('controller'=>'nacionalidades','action'=>'add')); ?></li>
                                    <li class="divider"></li>
                                    <li class="dropdown-header">Destinos</li>
                                   <li><?php echo $this->Html->link('Lista Destinos',array('controller'=>'destinos','action'=>'index')); ?></li>
                                   <li><?php echo $this->Html->link('Nuevo Destino',array('controller'=>'destinos','action'=>'add')); ?></li>
                                   <li class="divider"></li>
                                    <li class="dropdown-header">Tours</li>                                   
                                   <li><?php echo $this->Html->link('Nuevo Tour',array('controller'=>'tours','action'=>'add')); ?></li>
                                </ul>
                            </li>            
                           <li class="active"> <?php echo $this->Html->link('Contacto',array('controller' => 'pages', 'action' => 'display', 'contacto')); ?></li> 
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Bootstrap core JavaScript
            ================================================== -->
            <!-- Placed at the end of the document so the pages load faster -->
            <?php
           //echo $this->Html->script(array());
            ?>
        </body>
    </html>
