<div class="tours index">
	<h2><?php echo __('Tours'); ?></h2>
	<table  class="table" cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id_tour'); ?></th>
			<th><?php echo $this->Paginator->sort('fecha'); ?></th>
			<th><?php echo $this->Paginator->sort('hora_salida'); ?></th>
			<th><?php echo $this->Paginator->sort('lugar_salida'); ?></th>
			<th><?php echo $this->Paginator->sort('pax'); ?></th>
			<th><?php echo $this->Paginator->sort('destino_id'); ?></th>
			<th><?php echo $this->Paginator->sort('precio'); ?></th>
			<th><?php echo $this->Paginator->sort('saldo'); ?></th>
			<th><?php echo $this->Paginator->sort('cliente_id'); ?></th>
			<th><?php echo $this->Paginator->sort('hotel_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($tours as $tour): ?>
	<tr>
		<td><?php echo h($tour['Tour']['id_tour']); ?>&nbsp;</td>
		<td><?php echo h($tour['Tour']['fecha']); ?>&nbsp;</td>
		<td><?php echo h($tour['Tour']['hora_salida']); ?>&nbsp;</td>
		<td><?php echo h($tour['Tour']['lugar_salida']); ?>&nbsp;</td>
		<td><?php echo h($tour['Tour']['pax']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($tour['Destino']['destino'], array('controller' => 'destinos', 'action' => 'view', $tour['Destino']['id_destino'])); ?>
		</td>
		<td><?php echo h($tour['Tour']['precio']); ?>&nbsp;</td>
		<td><?php echo h($tour['Tour']['saldo']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($tour['Cliente']['id_cliente'], array('controller' => 'clientes', 'action' => 'view', $tour['Cliente']['id_cliente'])); ?>
		</td>
		<td><?php echo h($tour['Tour']['nombre_hotel']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $tour['Tour']['id_tour'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $tour['Tour']['id_tour'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $tour['Tour']['id_tour']), array('confirm' => __('Are you sure you want to delete # %s?', $tour['Tour']['id_tour']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
