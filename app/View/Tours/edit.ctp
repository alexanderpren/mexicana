<div class="tours form">
<?php echo $this->Form->create('Tour'); ?>
	<fieldset>
		<legend><?php echo __('Edit Tour'); ?></legend>
	<?php
		echo $this->Form->input('id_tour');
		echo $this->Form->input('fecha');
		echo $this->Form->input('hora_salida');
		echo $this->Form->input('lugar_salida');
		echo $this->Form->input('pax');
		echo $this->Form->input('destino_id');
		echo $this->Form->input('precio');
		echo $this->Form->input('saldo');
		echo $this->Form->input('cliente_id');
		echo $this->Form->input('hotel_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Tour.id_tour')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Tour.id_tour')))); ?></li>
		<li><?php echo $this->Html->link(__('List Tours'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Destinos'), array('controller' => 'destinos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Destino'), array('controller' => 'destinos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clientes'), array('controller' => 'clientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cliente'), array('controller' => 'clientes', 'action' => 'add')); ?> </li>
	</ul>
</div>
