<div class="tours view">
<h2><?php echo __('Tour'); ?></h2>
	<dl>
		<dt><?php echo __('Id Tour'); ?></dt>
		<dd>
			<?php echo h($tour['Tour']['id_tour']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fecha'); ?></dt>
		<dd>
			<?php echo h($tour['Tour']['fecha']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hora Salida'); ?></dt>
		<dd>
			<?php echo h($tour['Tour']['hora_salida']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lugar Salida'); ?></dt>
		<dd>
			<?php echo h($tour['Tour']['lugar_salida']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pax'); ?></dt>
		<dd>
			<?php echo h($tour['Tour']['pax']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Destino'); ?></dt>
		<dd>
			<?php echo $this->Html->link($tour['Destino']['destino'], array('controller' => 'destinos', 'action' => 'view', $tour['Destino']['id_destino'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Precio'); ?></dt>
		<dd>
			<?php echo h($tour['Tour']['precio']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Saldo'); ?></dt>
		<dd>
			<?php echo h($tour['Tour']['saldo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cliente'); ?></dt>
		<dd>
			<?php echo $this->Html->link($tour['Cliente']['id_cliente'], array('controller' => 'clientes', 'action' => 'view', $tour['Cliente']['id_cliente'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hotel Id'); ?></dt>
		<dd>
			<?php echo h($tour['Tour']['hotel_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Tour'), array('action' => 'edit', $tour['Tour']['id_tour'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Tour'), array('action' => 'delete', $tour['Tour']['id_tour']), array('confirm' => __('Are you sure you want to delete # %s?', $tour['Tour']['id_tour']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Tours'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tour'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Destinos'), array('controller' => 'destinos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Destino'), array('controller' => 'destinos', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clientes'), array('controller' => 'clientes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Cliente'), array('controller' => 'clientes', 'action' => 'add')); ?> </li>
	</ul>
</div>
