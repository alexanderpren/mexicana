<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
<?php echo $this->Form->create('Tour'); ?>
        <fieldset>
            <legend><?php echo __('Agregar  Tour'); ?></legend>
	<?php
		echo $this->Form->input('fecha',array('div'=>array('class'=>'input-group date','id'=>'divMiCalendario'),'class'=>'form-control','type'=>'text','id'=>'txtFecha','after' => '<span class="input-group-addo"><span class="glyphicon glyphicon-calendar"></span></span>'));
		echo $this->Form->input('hora_salida',array('div'=>array('class'=>'input-group clockpicker'),'class'=>'form-control','type'=>'text'));
                ?>
           
<!--                  <div class='input-group date' id='divMiCalendario'>
                      <input type='text' id="txtFecha" class="form-control"  readonly/>
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>-->
        
            <br>
            <?php
		echo $this->Form->input('lugar_salida');
		echo $this->Form->input('pax');
		echo $this->Form->input('destino_id');
		echo $this->Form->input('precio');
		echo $this->Form->input('saldo');
		echo $this->Form->input('cliente_id');
		echo $this->Form->input('hotel_id');
	?>
        </fieldset>
        <?php echo $this->Form->button('Agregar',array('type'=>'submit','class'=>'btn btn-primary')); ?>
    </div>
    <div class="col-md-4"></div>
</div>
<script type="text/javascript">
$('.clockpicker').clockpicker({
        align: 'left',
    donetext: 'Hecho'
});

$('#divMiCalendario').datetimepicker({
          format: 'DD-MM-YYYY'       
      });
//      $('#divMiCalendario').data("DateTimePicker").show();
//      $('datetimepicker').data("DateTimePicker").show();
</script>


   

