<div class="destinos view">
<h2><?php echo __('Destino'); ?></h2>
	<dl>
		<dt><?php echo __('Id Destino'); ?></dt>
		<dd>
			<?php echo h($destino['Destino']['id_destino']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Destino'); ?></dt>
		<dd>
			<?php echo h($destino['Destino']['destino']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Destino'), array('action' => 'edit', $destino['Destino']['id_destino'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Destino'), array('action' => 'delete', $destino['Destino']['id_destino']), array('confirm' => __('Are you sure you want to delete # %s?', $destino['Destino']['id_destino']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Destinos'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Destino'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tours'), array('controller' => 'tours', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tour'), array('controller' => 'tours', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Tours'); ?></h3>
	<?php if (!empty($destino['Tour'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id Tour'); ?></th>
		<th><?php echo __('Fecha'); ?></th>
		<th><?php echo __('Hora Salida'); ?></th>
		<th><?php echo __('Lugar Salida'); ?></th>
		<th><?php echo __('Pax'); ?></th>
		<th><?php echo __('Destino Id'); ?></th>
		<th><?php echo __('Precio'); ?></th>
		<th><?php echo __('Saldo'); ?></th>
		<th><?php echo __('Cliente Id'); ?></th>
		<th><?php echo __('Hotel Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($destino['Tour'] as $tour): ?>
		<tr>
			<td><?php echo $tour['id_tour']; ?></td>
			<td><?php echo $tour['fecha']; ?></td>
			<td><?php echo $tour['hora_salida']; ?></td>
			<td><?php echo $tour['lugar_salida']; ?></td>
			<td><?php echo $tour['pax']; ?></td>
			<td><?php echo $tour['destino_id']; ?></td>
			<td><?php echo $tour['precio']; ?></td>
			<td><?php echo $tour['saldo']; ?></td>
			<td><?php echo $tour['cliente_id']; ?></td>
			<td><?php echo $tour['hotel_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'tours', 'action' => 'view', $tour['id_tour'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'tours', 'action' => 'edit', $tour['id_tour'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tours', 'action' => 'delete', $tour['id_tour']), array('confirm' => __('Are you sure you want to delete # %s?', $tour['id_tour']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Tour'), array('controller' => 'tours', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
