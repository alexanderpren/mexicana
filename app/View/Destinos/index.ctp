<html>
<div class="bs-example" data-example-id="bordered-table">
    <h2><?php echo __('Destinos'); ?></h2>
<table  class="table" class="table table-bordered">
<thead>
<tr>
        <th><?php echo $this->Paginator->sort('id_destino','Id'); ?></th>
        <th><?php echo $this->Paginator->sort('destino','Destino'); ?></th>
        <th class="actions"><?php echo __('Acciones'); ?></th>
        
</tr>
</thead>
<tbody>
<?php foreach ($destinos as $destino): ?>
	<tr>
		<td><?php echo h($destino['Destino']['id_destino']); ?>&nbsp;</td>
		<td><?php echo h($destino['Destino']['destino']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Ver'), array('action' => 'view', $destino['Destino']['id_destino'])); ?>
			<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $destino['Destino']['id_destino'])); ?>
			<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $destino['Destino']['id_destino']), array('confirm' => __('Are you sure you want to delete # %s?', $destino['Destino']['id_destino']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>
    
    <p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
</html>

<div class="actions">
	<h3><?php echo __('Acciones'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Nuevo Destino'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('Lista de Tours'), array('controller' => 'tours', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('Nuevo Tour'), array('controller' => 'tours', 'action' => 'add')); ?> </li>
	</ul>
</div>
