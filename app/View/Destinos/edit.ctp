<div class="destinos form">
<?php echo $this->Form->create('Destino'); ?>
	<fieldset>
		<legend><?php echo __('Edit Destino'); ?></legend>
	<?php
		echo $this->Form->input('id_destino');
		echo $this->Form->input('destino');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Destino.id_destino')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Destino.id_destino')))); ?></li>
		<li><?php echo $this->Html->link(__('List Destinos'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Tours'), array('controller' => 'tours', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tour'), array('controller' => 'tours', 'action' => 'add')); ?> </li>
	</ul>
</div>
