


<div class="modal fade" id="loginusuario" tabindex="-1" role="dialog" aria-labelledby="loginusuarioLabel">
  <div class="modal-dialog" role="document">
    <?php
    echo $this->Form->create('Usuario',array('class'=>'form','role'=>'form'));
    ?>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="loginusuarioLabel">Login</h4>
      </div>
      <div class="modal-body">
        <?php
        echo $this->Form->input('nombre_usuario',array('class'=>'form-control'));
               echo $this->Form->input('password',array('class'=>'form-control'));
               ?>
      </div>
      <div class="modal-footer">
        <?php
        echo $this->Form->button('Login',array('type'=>'submit','class'=>'btn btn-default'));
        ?>
      </div>
    </div>
    <?php
    echo $this->Form->end();
    ?>
  </div>
</div>
<div class="row">
    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#loginusuario">
                        Acceder
                    </button>
</div>
