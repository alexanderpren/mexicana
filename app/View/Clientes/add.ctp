<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">

<?php echo $this->Form->create('Cliente'); ?>
	<fieldset>
		<legend><?php echo __('Agregar Cliente'); ?></legend>
	<?php
		echo $this->Form->input('nombre');
		echo $this->Form->input('nacionalidad_id');
		echo $this->Form->input('hotel_id');
		echo $this->Form->input('idioma_id');
	?>
	</fieldset>
<?php echo $this->Form->button('Agregar',array('type'=>'submit','class'=>'btn btn-primary')); ?>
    </div>
    <div class="col-md-4"></div>
</div>
