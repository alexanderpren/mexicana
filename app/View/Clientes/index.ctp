<div class="clientes index">
	<h2><?php echo __('Clientes'); ?></h2>
	<table class="table" cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id_cliente'); ?></th>
			<th><?php echo $this->Paginator->sort('nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('nacionalidad_id'); ?></th>
			<th><?php echo $this->Paginator->sort('hotel_id'); ?></th>
			<th><?php echo $this->Paginator->sort('idioma_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($clientes as $cliente): ?>
	<tr>
		<td><?php echo h($cliente['Cliente']['id_cliente']); ?>&nbsp;</td>
		<td><?php echo h($cliente['Cliente']['nombre']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($cliente['Nacionalidade']['nacionalidad'], array('controller' => 'nacionalidades', 'action' => 'view', $cliente['Nacionalidade']['id_nacionalidad'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($cliente['Hotele']['nombre_hotel'], array('controller' => 'hoteles', 'action' => 'view', $cliente['Hotele']['id_hotel'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($cliente['Idioma']['idioma'], array('controller' => 'idiomas', 'action' => 'view', $cliente['Idioma']['id_idioma'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $cliente['Cliente']['id_cliente'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $cliente['Cliente']['id_cliente'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $cliente['Cliente']['id_cliente']), array('confirm' => __('Are you sure you want to delete # %s?', $cliente['Cliente']['id_cliente']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

