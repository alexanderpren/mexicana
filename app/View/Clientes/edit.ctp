<div class="clientes form">
<?php echo $this->Form->create('Cliente'); ?>
	<fieldset>
		<legend><?php echo __('Edit Cliente'); ?></legend>
	<?php
		echo $this->Form->input('id_cliente');
		echo $this->Form->input('nombre');
		echo $this->Form->input('nacionalidad_id');
		echo $this->Form->input('hotel_id');
		echo $this->Form->input('idioma_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

