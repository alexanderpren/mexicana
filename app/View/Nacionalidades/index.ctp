<div class="nacionalidades index">
	<h2><?php echo __('Nacionalidades'); ?></h2>
	<table  class="table" cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id_nacionalidad'); ?></th>
			<th><?php echo $this->Paginator->sort('nacionalidad'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($nacionalidades as $nacionalidade): ?>
	<tr>
		<td><?php echo h($nacionalidade['Nacionalidade']['id_nacionalidad']); ?>&nbsp;</td>
		<td><?php echo h($nacionalidade['Nacionalidade']['nacionalidad']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $nacionalidade['Nacionalidade']['id_nacionalidad'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $nacionalidade['Nacionalidade']['id_nacionalidad'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $nacionalidade['Nacionalidade']['id_nacionalidad']), array('confirm' => __('Are you sure you want to delete # %s?', $nacionalidade['Nacionalidade']['id_nacionalidad']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
