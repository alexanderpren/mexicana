<div class="nacionalidades form">
<?php echo $this->Form->create('Nacionalidade'); ?>
	<fieldset>
		<legend><?php echo __('Edit Nacionalidade'); ?></legend>
	<?php
		echo $this->Form->input('id_nacionalidad');
		echo $this->Form->input('nacionalidad');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Nacionalidade.id_nacionalidad')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Nacionalidade.id_nacionalidad')))); ?></li>
		<li><?php echo $this->Html->link(__('List Nacionalidades'), array('action' => 'index')); ?></li>
	</ul>
</div>
