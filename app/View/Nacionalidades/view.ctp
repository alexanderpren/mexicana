<div class="nacionalidades view">
<h2><?php echo __('Nacionalidade'); ?></h2>
	<dl>
		<dt><?php echo __('Id Nacionalidad'); ?></dt>
		<dd>
			<?php echo h($nacionalidade['Nacionalidade']['id_nacionalidad']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nacionalidad'); ?></dt>
		<dd>
			<?php echo h($nacionalidade['Nacionalidade']['nacionalidad']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Nacionalidade'), array('action' => 'edit', $nacionalidade['Nacionalidade']['id_nacionalidad'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Nacionalidade'), array('action' => 'delete', $nacionalidade['Nacionalidade']['id_nacionalidad']), array('confirm' => __('Are you sure you want to delete # %s?', $nacionalidade['Nacionalidade']['id_nacionalidad']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Nacionalidades'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Nacionalidade'), array('action' => 'add')); ?> </li>
	</ul>
</div>
