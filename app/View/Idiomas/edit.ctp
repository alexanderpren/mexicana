<div class="idiomas form">
<?php echo $this->Form->create('Idioma'); ?>
	<fieldset>
		<legend><?php echo __('Edit Idioma'); ?></legend>
	<?php
		echo $this->Form->input('id_idioma');
		echo $this->Form->input('idioma');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Idioma.id_idioma')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Idioma.id_idioma')))); ?></li>
		<li><?php echo $this->Html->link(__('List Idiomas'), array('action' => 'index')); ?></li>
	</ul>
</div>
