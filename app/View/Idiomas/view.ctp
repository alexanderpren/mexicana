<div class="idiomas view">
<h2><?php echo __('Idioma'); ?></h2>
	<dl>
		<dt><?php echo __('Id Idioma'); ?></dt>
		<dd>
			<?php echo h($idioma['Idioma']['id_idioma']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Idioma'); ?></dt>
		<dd>
			<?php echo h($idioma['Idioma']['idioma']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Idioma'), array('action' => 'edit', $idioma['Idioma']['id_idioma'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Idioma'), array('action' => 'delete', $idioma['Idioma']['id_idioma']), array('confirm' => __('Are you sure you want to delete # %s?', $idioma['Idioma']['id_idioma']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Idiomas'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Idioma'), array('action' => 'add')); ?> </li>
	</ul>
</div>
